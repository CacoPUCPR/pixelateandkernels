import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Marcos on 02/04/17.
 */
public class Pixelate {


    float[][] blurKernel = {
            {1/16f, 2/16f, 1/16f},
            {2/16f, 4/16f, 2/16f},
            {1/16f, 2/16f, 2/16f},
    };

    float[][] sharpenKernel = {
            {0.0f, -1.0f, 0.0f},
            {-1.0f, 5.0f, -1.0f},
            {0.0f, -1.0f, 0.0f},
    };

    float[][] embossKernel = {
            {-2.0f, -1.0f, 0.0f},
            {-1.0f, 1.0f, 1.0f},
            {0.0f, 1.0f, 2.0f},
    };


    BufferedImage Pixelate(BufferedImage img, int pixelSize){

        BufferedImage out = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);

        for (int xPosition = 0; xPosition < img.getWidth(); xPosition++) {
            for (int yPosition = 0; yPosition < img.getHeight(); yPosition++) {

                int roundedXPosition = xPosition/pixelSize * pixelSize;
                int roundedYPosition = yPosition/pixelSize * pixelSize;

                out.setRGB(xPosition, yPosition, img.getRGB(roundedXPosition, roundedYPosition));
            }
        }
        return out;
    }

    BufferedImage applyEffect(BufferedImage img, float[][] kernel){

        BufferedImage out = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);

        for (int xPosition = 0; xPosition < img.getWidth(); xPosition++) {
            for (int yPosition = 0; yPosition < img.getHeight(); yPosition++) {

                int r = 0, g = 0, b = 0;

                for (int kernelX = 0; kernelX < 3; kernelX++){

                    for (int kernelY = 0; kernelY < 3; kernelY++) {

                        int centerX = xPosition + (kernelX - 1); //making the points in center of kernel
                        int centerY = yPosition + (kernelY - 1);

                        if(!(centerX < 0 || centerX >= img.getWidth() || centerY < 0 || centerY >= img.getHeight())) {

                            Color newColor = new Color(img.getRGB(centerX, centerY));

                            r += newColor.getRed() * kernel[kernelX][kernelY];
                            g += newColor.getGreen() * kernel[kernelX][kernelY];
                            b += newColor.getBlue() * kernel[kernelX][kernelY];
                        }


                    }
                }

                Color satColor = new Color(saturate(r), saturate(g), saturate(b));

                out.setRGB(xPosition,yPosition, satColor.getRGB());
            }
        }
        return out;
    }

    int saturate(int value){
        if(value > 255)
            return 255;
        if(value < 0)
            return 0;
        return value;
    }

    public void run() throws IOException {

        BufferedImage image = ImageIO.read(new File("src/puppy.png"));
        BufferedImage pixImg = Pixelate(image, 10);
        BufferedImage sharpImg = applyEffect(pixImg, sharpenKernel);
        BufferedImage embossImg = applyEffect(image, embossKernel);
        BufferedImage blurImg = applyEffect(image, blurKernel);

        ImageIO.write(pixImg, "png", new File("pixel_puppy.png"));
        ImageIO.write(sharpImg, "png", new File("sharpen_pixel_puppy.png"));
        ImageIO.write(embossImg, "png", new File("emboss_puppy.png"));;
        ImageIO.write(blurImg, "png", new File("blurried_puppy.png"));


    }



    static public void main (String[] args) throws IOException {
        new Pixelate().run();
    }
}
